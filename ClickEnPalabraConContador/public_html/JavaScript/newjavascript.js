var arrayPalabras = ["Puede", "que", "si", "pulsas", "aquí", "50 veces", "pase", "algo"];
var cont = 1;
var indice = 0;

function pulsacion() {
    document.getElementById("contador").innerHTML = cont++;  // "contador" es la id que tiene el div que contiene el contador en el html y hace que vaya cambiando
    var f = document.getElementById("zonaPalabra");  // en f se guarda el div cuya id es zonaPalabra

    // variables con números aleatorios para dar color a la palabra. cada una es una propiedad del RGB.
    var k = Math.floor(Math.random() * 256);
    var l = Math.floor(Math.random() * 256);
    var m = Math.floor(Math.random() * 256);
    f.style.color = "rgb(" + k + "," + l + "," + m + ")";  // dar color a palabra

    f.innerHTML = arrayPalabras[indice];  // mostrar en el html la palabra elegida      

    // rotar el indice de palabras para que vuelva a empezar cuando llegue al final
    if (indice < arrayPalabras.length - 1) {
        indice++;
    } else {
        indice = 0;
    }

    if (cont === 51) {
        limpiarYponerImagen();
    }
}

function limpiarYponerImagen() {
    document.getElementById("body").innerHTML =
            "<div><img src=\"http://i.imgur.com/aQI83F6.gif\"></div>\n\
            <div><button onclick=\"javascript:window.location.reload();\">Atrás</button></div>";  // las barras \ sin para tratar las comillas como string
}

function instrucciones() {
    alert("Haz click en la palaba flotante para cambiarla y aumentar el contador");
}
